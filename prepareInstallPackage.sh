#!/bin/sh

set -euo pipefail

if [ -z $1 ] || [ -z $2 ]
then
    echo "Wrong param!"
    exit 1
fi

pyVer=$1
package=$2

# remove old upacked source package
rm -rf iptvplayer-for-e2.git
rm -rf usr/

# get package
if [ ! -f $package ]
then 
	wget http://www.iptvplayer.gitlab.io/update2/$package
fi

# unpack source package
tar -xf $package
rm $package

# remove not used files
rm -rf iptvplayer-for-e2.git/IPTVPlayer/icons/PlayerSelector/*135.png
rm -rf iptvplayer-for-e2.git/IPTVPlayer/icons/PlayerSelector/*120.png
rm -rf iptvplayer-for-e2.git/IPTVPlayer/icons/PlayerSelector/marker165.png
rm -rf iptvplayer-for-e2.git/IPTVPlayer/icons/PlayerSelector/marker180.png

# remove old folder
rm -rf usr

# create Enigma2 default Extensions path
mkdir -p usr/lib/enigma2/python/Plugins/Extensions/

# copy IPTVPlayer to Extensions path
cp -R iptvplayer-for-e2.git/IPTVPlayer/ usr/lib/enigma2/python/Plugins/Extensions/

# create install package
GZIP=-9 tar -zcf "iptvplayer_python"$pyVer".tar.gz" usr

# remove old folder
rm -rf usr
rm -rf iptvplayer-for-e2.git

sync
